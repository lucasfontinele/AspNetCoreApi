﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpApi.Application.Configuration;
using Microsoft.AspNetCore.Mvc;
using MySql.Data.MySqlClient;

namespace CSharpApi.Controllers
{
    [Route("api/[controller]")]
    public class ValuesController : Controller
    {
        // GET api/values
        [HttpGet]
        public IEnumerable<string> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public List<String> Get(int id)
        {
            List<String> names = new List<string>();

            String sql = "SELECT name FROM users";

            MySqlCommand cmd = new MySqlCommand(sql, Database.connection);
            MySqlDataReader Reader = cmd.ExecuteReader();

            while (Reader.Read())
            {
                names.Add(Reader["name"].ToString());
            }

            return names;
        }

        // POST api/values
        [HttpPost]
        public void Post([FromBody]string value)
        {
        }

        // PUT api/values/5
        [HttpPut("{id}")]
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE api/values/5
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
