﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpApi.Application.Util;
using MySql.Data.MySqlClient;

namespace CSharpApi.Application.Configuration
{
    public class Database
    {
        private String Server;
        private String Data;
        private String Username;
        private String Password;
        private uint Port;

        public static MySqlConnection connection;
        private static MySqlConnectionStringBuilder strBuilder;

        public Database()
        {
            IniFile iniFile = new IniFile($"{System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/Settings.ini");

            Server = iniFile.Read("Server", "MySQL");
            Data = iniFile.Read("Database", "MySQL");
            Username = iniFile.Read("Username", "MySQL");
            Password = iniFile.Read("Password", "MySQL");
            Port = Convert.ToUInt32(iniFile.Read("Port", "MySQL"));


            strBuilder = new MySqlConnectionStringBuilder
            {
                Server = Server,
                Database = Data,
                UserID = Username,
                Password = Password,
                Port = Port,
                SslMode = MySqlSslMode.None
            };            

            connection = new MySqlConnection(strBuilder.ToString());

            connection.Open();
        }
    }
}