﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using CSharpApi.Application.Configuration;
using CSharpApi.Application.Util;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace CSharpApi
{
    public class Program
    {
        private static IniFile iniFile;

        public static void Main(string[] args)
        {
            Configuration();

            var database = new Database();

            BuildWebHost(args).Run();
        }

        private static void Configuration()
        {
            if (!File.Exists($"{System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/Settings.ini"))
            {
                iniFile = new IniFile($"{System.IO.Path.GetDirectoryName(Assembly.GetEntryAssembly().Location)}/Settings.ini");

                iniFile.Write("Server", "", "MySQL");
                iniFile.Write("Database", "", "MySQL");
                iniFile.Write("Username", "", "MySQL");
                iniFile.Write("Password", "", "MySQL");
                iniFile.Write("Port", "", "MySQL");
            }
        }

        public static IWebHost BuildWebHost(string[] args) =>
            WebHost.CreateDefaultBuilder(args)
                .UseStartup<Startup>()
                .Build();
    }
}
